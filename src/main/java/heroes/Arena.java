package main.java.heroes;

import main.java.heroes.generals.IGeneralStrategy;
import main.java.heroes.generals.LeftSideGeneral;
import main.java.heroes.generals.RightSideGeneral;

public class Arena {

    private IGeneralStrategy rightGeneral, leftGeneral;

    public Arena(int totalExperiencePoints) {
        rightGeneral = new RightSideGeneral(totalExperiencePoints);
        leftGeneral = new LeftSideGeneral(totalExperiencePoints);
    }

    public void war(int numberOfMovesForGenerals){

        while(true){

            rightGeneral.fightGeneral(leftGeneral,numberOfMovesForGenerals);
            if(leftGeneral.getList().isEmpty()){
                System.out.println("Right General win this battle!");
                return;
            }
            leftGeneral.fightGeneral(rightGeneral,numberOfMovesForGenerals);
            if(rightGeneral.getList().isEmpty()){
                System.out.println("Left General win this battle!");
                return;
            }
        }
    }
}