package main.java.heroes.soldiers;

public class ArcherSoldier extends AbstractSoldier {

    public ArcherSoldier(int exp) {
        experience_points = exp;
        archDefenceLevel = 10;
        handFightDefenceLevel = 8;
        magicDefenceLevel = 7;
        health_points = getHealth_points();
        type = ESoldierType.ARCHER;
    }
}
