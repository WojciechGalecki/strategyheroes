package main.java.heroes.soldiers;

public class KnightSoldier extends AbstractSoldier {

    public KnightSoldier(int exp) {
        experience_points = exp;
        archDefenceLevel = 7;
        handFightDefenceLevel = 10;
        magicDefenceLevel = 8;
        health_points = getHealth_points();
        type = ESoldierType.KNIGHT;
    }
}
