package main.java.heroes.soldiers;

public class MageSoldier extends AbstractSoldier {

    public MageSoldier(int exp) {
        experience_points = exp;
        archDefenceLevel = 8;
        handFightDefenceLevel = 7;
        magicDefenceLevel = 10;
        health_points = getHealth_points();
        type = ESoldierType.MAG;
    }
}
