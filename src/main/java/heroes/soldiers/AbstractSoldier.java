package main.java.heroes.soldiers;

public abstract class AbstractSoldier {

    protected double health_points = 10.0;
    protected int experience_points;
    protected int archDefenceLevel;
    protected int handFightDefenceLevel;
    protected int magicDefenceLevel;
    protected ESoldierType type;

    public double getHealth_points() {
        return health_points;
    }

    public ESoldierType getType() {
        return type;
    }

    public boolean fight(AbstractSoldier soldierToFight) {
        boolean isKilled;
        int defenseLevel;
        if (type == ESoldierType.ARCHER) {
            defenseLevel = soldierToFight.archDefenceLevel;
        } else if (type == ESoldierType.KNIGHT) {
            defenseLevel = soldierToFight.handFightDefenceLevel;
        } else {
            defenseLevel = soldierToFight.magicDefenceLevel;
        }
        double currentHealth = (experience_points * 10) - (defenseLevel * 2 * soldierToFight.experience_points);
        // minimum 5 points
        if(currentHealth < 6) {
            currentHealth = 5;
        }
        if (soldierToFight.health_points <= currentHealth) {
            soldierToFight.health_points = 0.0;
            isKilled = true;
        } else {
            soldierToFight.health_points = soldierToFight.health_points - currentHealth;
            isKilled = false;
        }
        return isKilled;
    }

}
