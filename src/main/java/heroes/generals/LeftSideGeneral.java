package main.java.heroes.generals;

import main.java.heroes.soldiers.AbstractSoldier;
import main.java.heroes.soldiers.ArcherSoldier;
import main.java.heroes.soldiers.KnightSoldier;
import main.java.heroes.soldiers.MageSoldier;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LeftSideGeneral implements IGeneralStrategy {

    private List<AbstractSoldier> soldierList;

    public LeftSideGeneral(int totalExperiencePoints) {
        this.soldierList = generateSoldier(totalExperiencePoints);
    }

    @Override
    public List<AbstractSoldier> getList() {
        return soldierList;
    }

    @Override
    public List<AbstractSoldier> generateSoldier(int experiencePoints) {
        List<AbstractSoldier> createdSoldiers = new ArrayList<>();
        Random random = new Random();
        int numberOfArchersExp = random.nextInt(experiencePoints) + 1;
        int numberOfKnightsExp = 0;
        int numberOfMagesExp = 0;
        if (numberOfArchersExp < experiencePoints) {
            numberOfKnightsExp = random.nextInt(experiencePoints - numberOfArchersExp) + 1;
        }
        if (numberOfArchersExp + numberOfKnightsExp < experiencePoints) {
            numberOfMagesExp = experiencePoints - numberOfArchersExp - numberOfKnightsExp;
        }
        while (numberOfArchersExp > 0) {
            int singleSoldierExp = random.nextInt(numberOfArchersExp) + 1;
            createdSoldiers.add(new ArcherSoldier(singleSoldierExp));
            numberOfArchersExp -= singleSoldierExp;
        }

        while (numberOfKnightsExp > 0) {
            int singleSoldierExp = random.nextInt(numberOfKnightsExp) + 1;
            createdSoldiers.add(new KnightSoldier(singleSoldierExp));
            numberOfKnightsExp -= singleSoldierExp;
        }

        while (numberOfMagesExp > 0) {
            int singleSoldierExp = random.nextInt(numberOfMagesExp) + 1;
            createdSoldiers.add(new MageSoldier(singleSoldierExp));
            numberOfMagesExp -= singleSoldierExp;
        }

        return createdSoldiers;
    }

    @Override
    public void fightGeneral(IGeneralStrategy secondGeneral, int numberOfMoves) {
        // simple game implementation
        for (int i = 0; i < numberOfMoves; i++) {
            if (soldierList.isEmpty()) {
                return;
            } else {
                boolean killStatus = soldierList.get(0).fight(secondGeneral.getList().get(0));
                if (killStatus == true) {
                    System.out.println(secondGeneral.getList().get(0).getType() + " was killed!");
                    secondGeneral.getList().remove(0);
                } else {
                    System.out.println(secondGeneral.getList().get(0).getType() + " health - "
                            + secondGeneral.getList().get(0).getHealth_points());
                }
            }
        }
    }
}



