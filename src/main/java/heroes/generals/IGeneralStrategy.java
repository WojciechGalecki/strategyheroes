package main.java.heroes.generals;

import main.java.heroes.soldiers.AbstractSoldier;
import java.util.List;

public interface IGeneralStrategy {

    List<AbstractSoldier> generateSoldier(int experiencePoints);

    List<AbstractSoldier> getList();

    void fightGeneral(IGeneralStrategy secondGeneral, int numberOfMoves);


}
